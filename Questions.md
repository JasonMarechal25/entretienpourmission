# Technologies
* De quoi est composé votre stack technologique ?
    * Pourquoi avoir fait ces choix <=> Attendu: Une raison significative pas juste "C'est décidé au niveau groupe". Même si c'est décidé au niveau groupe des arguments pertinents sont attendus.

# Projets
* Qui est le client ? <=> Interne ou Externe
* Comment communiquez vous avec le client ?
* Comment travaillez vous avec le client ?

# Autres
* Etes vous ouvert azux suggestions ? (Outils, méthode techniques, méthode projet)
    * Vraiment ?
    * Etes vous prêt à expérimenter ?
* Comment maintenez-vous à jour les compétences de vos équipes ? <=> Y a t-il des BBL, des temps dédiés à des démo ou des discutions techniques...
* Qu'est-ce qui vous motive à vous lever pour venir travailler ?
* Puis-je rencontrer l'équipe ?
* Comment travaillez vous au quotidien ?
* Quel est le cycle de vie d'un backlog item. De l'idée du stakeholder jusqu'à la mise en prod, par où ca passe, et combien de temps ça met.
