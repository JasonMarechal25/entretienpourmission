Participation aux décisions techniques et technologique
Accompagnement de l'équipe sur les pratiques craft (TDD, DDD, ...)
# Objectif professionel
Apprendre de nouvelles pratiques ou de nouvelles technologies
Mettre en pratique mes compétences (TDD, design, Agilité)
# Cadre de travail
## Environement
Télétravail 100%
## Technique
### Outils
Environnement à l'état de l'art
* IDE à jour et configuré
* CI/CD complète
* Outillage et environnement de test et d'éxécution simple et fonctionnel
Ou forte volonté et plan d'action pour y arriver.
### Pratiques
* TDD présent ou appeler pour aider à le mettre en place
### Langage
Pas de C++ (Ca ça va être compliqué à cours terme). Ras le bol d'un langage ou on met 3 plombe à faire quoi que ce soit.
Langages/Techno de préférence (Dans l'ordre):
* Angular/React
* Scala/Kotlin/Rust/Go
* C#/Java
## Projet
Pas d'hypocrisie sur les méthodes de travail. Exemple, si vous dites que vous faites Scrum, faites le vraiement. Sinon dites que vous faites autre chose ou carrément du cycle en V
Implication des développeurs dans le produit. Les développeurs ne sont pas juste des exécutant à la tâche.
## Organisation
Transparence et écoute lorsque l'on souhaites proposer des améliorations. Les personnes compétentes sur les sujets doivent être facilement identifiables. Exemple: Si je trouve qu'un outils ne convient pas j'aimerais pouvoir trouver les motivation qui ont conduis à choisir cet outils et/ou communiquer facilement avec la personne qui pourrait m'en dire plus ou que je pourrait convaincre de changer d'outils.
